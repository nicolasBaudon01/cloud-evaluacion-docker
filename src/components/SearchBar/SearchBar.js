import React from "react";
import "./SearchBar.css";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      term: "",
      location: "",
      sortBy: "best_match",
      searching: false,
    };
    this.sortByOptions = {
      "Mejor Resultado": "best_match",
      "Mejor Puntuación": "rating",
      "Más Relevante": "review_count",
    };
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleTermChange = this.handleTermChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  handleSortByChange(sortByOption) {
    this.setState({ sortBy: sortByOption });
  }

  handleTermChange(e) {
    let term = e.target.value;
    this.setState({ term: term });
  }

  handleLocationChange(e) {
    let location = e.target.value;
    this.setState({ location: location });
  }

  getSortByClass(sortByOption) {
    if (this.state.sortBy === sortByOption) {
      return "selected";
    } else {
      return "";
    }
  }

  renderSortByOptions() {
    return Object.keys(this.sortByOptions).map((sortByOption) => {
      let sortByOptionValue = this.sortByOptions[sortByOption];
      return (
        <li
          className={this.getSortByClass(sortByOptionValue)}
          onClick={this.handleSortByChange.bind(this, sortByOptionValue)}
          key={sortByOptionValue}
        >
          {sortByOption}
        </li>
      );
    });
  }

  handleSearch(e) {
    if (this.state.location !== "" && this.state.term !== "") {
      this.setState({ searching: true });
      this.props
        .searchYelp(this.state.term, this.state.location, this.state.sortBy)
        .then(() => {
          this.setState({ searching: false });
        });
      e.preventDefault();
    } else {
      alert("¡Completá ambos campos para realizar la búsqueda!");
    }
  }

  render() {
    return (
      <div className="SearchBar">
        <div className="SearchBar-sort-options">
          <ul>{this.renderSortByOptions()}</ul>
        </div>
        <div className="SearchBar-fields">
          <input
            placeholder="Comida deseada"
            onChange={this.handleTermChange}
          />
          <input placeholder="Ubicación" onChange={this.handleLocationChange} />
        </div>
        <div className="SearchBar-submit">
          <a onClick={this.handleSearch}>
            {this.state.searching ? "Buscando..." : "Buscar"}
          </a>
        </div>
      </div>
    );
  }
}

export default SearchBar;
